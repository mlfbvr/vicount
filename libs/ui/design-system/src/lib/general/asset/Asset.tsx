import React, { memo, lazy, Suspense, useMemo } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';

import { InferPropTypes } from '@vicount/shared/types';

import classNames from './Asset.module.scss';

export const ASSETS = {
  COWORKING: 'undraw/coworking',
};

const propTypes = {
  /** ClassName. */
  className: PropTypes.string,
  /** Asset name -> use Asset.ICONS.NAME. */
  asset: PropTypes.oneOf(Object.values(ASSETS)),
  /** Asset primary color. */
  primaryColor: PropTypes.string,
  /** Asset secondary color. */
  secondaryColor: PropTypes.string,
};

const defaultProps = {
  className: null,
  asset: ASSETS.COWORKING,
  primaryColor: null,
  secondaryColor: null,
};

export type AssetProps = InferPropTypes<typeof propTypes, typeof defaultProps>;

const importIcon = (asset) =>
  import(`!!@svgr/webpack?-svgo,+titleProp,+ref!./assets/${asset}.svg`);

export const AssetBase: React.FC<AssetProps> = ({
  className,
  asset,
  primaryColor,
  secondaryColor,
  ...props
}) => {
  const ImportedAsset = useMemo(() => lazy(() => importIcon(asset)), [asset]);

  const cn = clsx(className);

  return (
    <Suspense fallback={<div className={clsx(cn, classNames.placeholder)} />}>
      <ImportedAsset
        {...props}
        className={cn}
        style={{
          color: primaryColor,
          fill: secondaryColor || primaryColor,
        }}
      />
    </Suspense>
  );
};

AssetBase.displayName = 'AssetBase';

AssetBase.propTypes = propTypes;
AssetBase.defaultProps = defaultProps;

export const Asset = memo(AssetBase);
