module.exports = () => ({
  name: 'loaders',
  configureWebpack() {
    return {
      module: {
        rules: [
          {
            test: /\.(mermaid)$/i,
            exclude: /\.(mdx?)$/i,
            use: ['raw-loader'],
          },
        ],
      },
    };
  },
});
