---
slug: model
title: Model
sidebar_label: Model
---

import Mermaid from '@theme/Mermaid';
import architecture from './graphs/architecture.mermaid';
import tenantsClassDiagram from './graphs/tenants.offices.rooms.class.mermaid';

## Architecture

<Mermaid chart={architecture} />

## Model

<Mermaid chart={tenantsClassDiagram} />
