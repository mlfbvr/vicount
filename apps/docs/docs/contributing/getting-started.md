---
slug: getting-started
title: Getting Started
sidebar_label: Getting Started
---

## Start your local environment

In order to start developing on Vicount, you will need to start a local environment.

There are a few prerequisites:

- Docker
- Helm & kubernetes-cli
- Minikube
- Terraform

You can then start the environment by starting minikube and using terraform to create the required services :

```shell
cd _infra \
  && minikube start \
  && terraform init \
  && terraform apply -auto-approve
```
