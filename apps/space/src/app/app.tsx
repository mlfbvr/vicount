import React from 'react';
import { Asset, ASSETS } from '@vicount/ui/design-system';

export const App = () => {
  return (
    <>
      <div style={{ textAlign: 'center' }}>
        <h1 className="text-indigo-500">Welcome to space!</h1>
        <Asset asset={ASSETS.COWORKING} className="w-64 h-64" />
      </div>
    </>
  );
};

export default App;
