import * as React from 'react';

export default ({ ...props }) => React.createElement('svg', props);
