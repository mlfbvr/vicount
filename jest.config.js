module.exports = {
  projects: [
    '<rootDir>/apps/space',
    '<rootDir>/libs/ui/design-system',
    '<rootDir>/libs/shared/types',
  ],
  moduleNameMapper: {
    '\\.svg': '<rootDir>/__mocks__/svgrMock.js',
  },
};
