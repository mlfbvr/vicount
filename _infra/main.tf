# Modules

## k8s

### Monitoring

module "kubernetes_monitoring" {
  source = "./modules/monitoring"

  count = var.enable_kubernetes_monitoring ? 1 : 0

  providers = {
    helm = helm
  }
}

module "kubernetes_api_gateway_ingress" {
  source = "./modules/api-gateway"

  count = var.enable_kubernetes_api_gateway_ingress ? 1 : 0

  providers = {
    helm = helm
  }
}

module "kubernetes_monitoring_api_gateway_ingress" {
  source = "./modules/monitoring-api-gateway"

  count = var.enable_kubernetes_monitoring && var.enable_kubernetes_api_gateway_ingress ? 1 : 0

  providers = {
    kubernetes-alpha = kubernetes-alpha
  }

  depends_on = [
    module.kubernetes_monitoring,
    module.kubernetes_api_gateway_ingress
  ]
}

# Apps

## Space

# module "app_space" {
#   source = "./../apps/space/_infra"
#   count = var.enable_app_space ? 1 : 0
# }


