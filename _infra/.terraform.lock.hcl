# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/helm" {
  version     = "2.1.2"
  constraints = ">= 2.1.2"
  hashes = [
    "h1:OdGovuq6XVk3+qzZ685mzZI0Ji2Iafa2jCaczJpDLiA=",
    "zh:09bd2b6f33a040c3fd59d82c9768b886b8c82163e31ec92dc1b747229d0548df",
    "zh:09f209fa57ad5d01f04c458f1719b42958ca5e0fc2eca63d9ec29f92c77a29f8",
    "zh:0bfc627539500ffb2a41a2f8a5ea7f6fb1d76367b11bbf9489b483b9e8dfff8f",
    "zh:0c0fef5587a5e927d15f9f4cc13cd0620b138238f9a422490fe9ea2bf086b61a",
    "zh:187f99648fad2b84d49cdd372f8f6cedbf06e13411b3f1ff66708f66852d7855",
    "zh:3d9ae08f8a99b19e80bd27708aecf592c28c92da66fd60189dfd7dce4d7da93c",
    "zh:60b767109362c616b2e6386bfb08581b03bc3e528920444e52b16743f5a180d6",
    "zh:729db42ed49d91c9b51eb602b9253e6ed6b3ab613c42deefc14996c9a8ee8ae4",
    "zh:8401f3bf6d69ce43eb14911823c7e5cbb273cf564508043cd04fb064c30a3e1a",
    "zh:91139b492ce1f41847017349ea49f9441b7cf70762c8d1c32a6a909e25ed10c1",
    "zh:98fca606a539510edc94dcad8069a321e6a42df90e483f58df03b305726d9220",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.2.0"
  constraints = ">= 2.2.0"
  hashes = [
    "h1:MP2ApCv4RbHWreeKQ+0xZZHALJ94AZIkfjiPsLRN41I=",
    "zh:41be89f07c279425a146a31c2cca646efab65f4d5f0b8bbd6059761ff0446231",
    "zh:4cbbb28396dc766bfca7ee2dbc19fa57331754eca7fee0fe57a87a73f5010f20",
    "zh:5df93be3e696d60139a485a2937176739f05ac1884850bc58839c3ed99cff995",
    "zh:64f01bcfc2ad1a42fbd307b9ceb7f70b3c19b35cdd0b950005190ec2de41083a",
    "zh:7fc88030e20dd9560fd0e64c094d07d6f0c8d1db7838f77dc278f3cae93e7207",
    "zh:81f7a855f1de2859c83565bc278869f6c7565b56d966fe9a4ef59fb55e5366d8",
    "zh:d74811f3d3112ab52d151e1e1fa7fc9087d0ea5cc9ef72484c612780890ee586",
    "zh:e93c93c43445855b4a75ec754b8c1579c55f95c23c1a33c188a57aa38357e513",
    "zh:f67b32a26cbb763134f6294a2b95798d63aacc63532457c10dc779819f6d8ad0",
    "zh:fb435298e7b0d8a021bdbfa496dd5c76c9eb0dada4b406b3439949bd4aef756a",
    "zh:ffafbdea9766b89525c67886f6f0ee9992ae48a229c689716952d71a06b05107",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes-alpha" {
  version     = "0.4.1"
  constraints = "0.4.1"
  hashes = [
    "h1:KGRBxJtYbPx55jxCtTpQas81ZMHadtOtWkzMgZPstks=",
    "zh:23295e5af307f47dc52aeac561267ec053e0284bd1833add764adf88d38f3cdf",
    "zh:2ba88eb177842f78730b2c235429a139753c192df7c74392d37f4eb0f22b964a",
    "zh:3f34bbc0868b9b8b4a83008c178a4a29ccc874ad6caa9ec15b603fc6d24fae33",
    "zh:460d5b379d6cb54676850cbe74f79a6b3532cea2a95e50a146e015ed2cd3fc9d",
    "zh:6df768b43ca272613c706a913efe407a7b5e536814d0e291d9c0d76ab69ed37e",
    "zh:804f12b58bcd5c5c96dc54f8aa1c80f63202dc8070e623be95af573b9eef22de",
    "zh:8074857fa72562a2ef25684a39f671cfd6892229db9c3a3e325159f4ecf04704",
    "zh:8e0a0cc39665cb79f85a51246a75fb6b316533066f129b57d93c209de161b507",
    "zh:9c181872eb09ff7f0b320be85a8ece278ea6e44a14b65dc09c72fa73a4517ae9",
    "zh:c3e0808ea082da9937c0329dc53c9ba56441296bf44898efad6937dcd921e159",
    "zh:ded165fa38f8dce64c7e6197e1852152cb8b13662cd1f996ce69986c22679e9c",
  ]
}
