resource "helm_release" "api-gateway_kong" {
  name       = "kong"
  chart      = "kong"
  repository = local.repositories.kong.url

  values = [
    file("${local.root}/values/kong.yml")
  ]

  namespace        = local.namespace
  create_namespace = true

  lint = true

  set {
    name  = "ingressController.installCRDs"
    value = false
  }
}
