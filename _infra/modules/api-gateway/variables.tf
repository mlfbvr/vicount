# Kubernetes

locals {
  root = abspath(path.module)

  namespace = "kong"

  repositories = {
    kong = {
      url = "https://charts.konghq.com"
    }
  }
}
