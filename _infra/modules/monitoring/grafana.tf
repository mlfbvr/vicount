resource "helm_release" "monitoring_grafana" {
  name       = "grafana"
  chart      = "grafana"
  repository = local.repositories.grafana.url

  values = [
    file("${local.root}/values/grafana.yml")
  ]

  namespace        = local.namespace
  create_namespace = true

  lint = true
}
