# Kubernetes

locals {
  root = abspath(path.module)

  namespace = "monitoring"

  repositories = {
    prometheus-community = {
      url = "https://prometheus-community.github.io/helm-charts"
    }

    grafana = {
      url = "https://grafana.github.io/helm-charts"
    }
  }
}
