resource "helm_release" "monitoring_prometheus" {
  name       = "prometheus"
  chart      = "prometheus"
  repository = local.repositories.prometheus-community.url

  values = [
    file("${local.root}/values/prometheus.yml")
  ]

  namespace        = local.namespace
  create_namespace = true

  lint = true
}
