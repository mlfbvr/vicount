resource "kubernetes_manifest" "kong-plugin-prometheus" {
  provider = kubernetes-alpha

  manifest = {
    "apiVersion" = "configuration.konghq.com/v1"
    "kind"       = "KongClusterPlugin"
    "plugin"     = "prometheus"

    "metadata" = {
      "name" = "prometheus"
      "annotations" = {
        "kubernetes.io/ingress.class" = "kong"
      }

      "labels" = {
        "global" = "true"
      }
    }
  }
}
