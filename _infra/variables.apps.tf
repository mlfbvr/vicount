# Apps

variable "enable_app_space" {
  type        = bool
  default     = false
  description = "Flag to enable Space app on our infra"
}
