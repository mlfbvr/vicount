# Kubernetes

variable "kubernetes_cluster" {
  type        = string
  default     = "minikube"
  description = "Kubernetes target cluster"
}

variable "kubernetes_config_path" {
  type        = string
  default     = "~/.kube/config"
  description = "Kubernetes config path"
}

## Monitoring

variable "enable_kubernetes_monitoring" {
  type        = bool
  default     = true
  description = "Flag to enable kubernetes monitoring module"
}

## API Gateway / Ingress

variable "enable_kubernetes_api_gateway_ingress" {
  type        = bool
  default     = true
  description = "Flag to enable kubernetes api gateway as ingress module"
}

##

# Dependencies

## Databases

## IdP

## Link IdP to API GW
