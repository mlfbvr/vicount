module.exports = {
  '*.{js,json,css,scss,md,ts,html,graphql}': ['yarn format --uncommitted'],
  '*.{tf,tfvars}': ['_bin/lint-staged-terraform-fmt.sh'],
};
